#-----------------------
#SSH Key
#-----------------------
ssh_name            = "BW-BMC-POC"
ssh_default         = true

#-----------------------
#Node Infor
#-----------------------
node_hostname       = "abvs-saltbox"
node_os             = "ubuntu/focal"
node_type           = "s0.d1.small"
node_loc            = "PHX"
