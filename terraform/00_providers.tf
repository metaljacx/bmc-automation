terraform {
  backend "http" {}
  required_providers {
    pnap = {
      source = "phoenixnap/pnap"
      version = "0.9.1"
    }
    rke = {
      source  = "rancher/rke"
      version = "1.2.3"
    }
    null = {
      source = "hashicorp/null"
      version = "3.1.0"
    }
  }
}

provider "pnap" {
  client_id = var.pnapid
  client_secret = var.pnapsec
}

provider "rke" {
  debug = true
  log_file = "rke_debug.log"
}
