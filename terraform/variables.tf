#-----------------------
#Terraform Providers
#-----------------------
variable "pnapid"   {type = string}
variable "pnapsec"  {type = string}
variable "be_password" {type = string}
#variable "bear" {type = string}

#-----------------------
#BMC Servers
#-----------------------
variable "res_id" {
    type = string
    sensitive = true
    }
variable "node_hostname" {type = string}
variable "node_os" {type = string}
variable "node_type" {type = string}
variable "node_loc" {type = string}
variable "node_ssh_default" {
    type = string 
    default = true
}
#variable "node_ssh_keys" {type = list}

#-----------------------
#BMC SSH
#-----------------------
variable "ssh_name" {type = string}
variable "ssh_default" {
    type = string
    default = false
}
variable "ssh_key" {
    type = string
    sensitive = true
}

#-----------------------
#CI/CD
#-----------------------
variable "git_project_path" {type = string}
