resource "pnap_ssh_key" "ssh-key" {
    name            = var.ssh_name
    default         = var.ssh_default 
    key             = var.ssh_key
}

