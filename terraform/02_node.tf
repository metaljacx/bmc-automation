resource "pnap_server" "BMCNodes" {
  hostname                    = var.node_hostname
  os                          = var.node_os
  type                        = var.node_type
  location                    = var.node_loc
  reservation_id              = var.res_id
  install_default_ssh_keys    = var.node_ssh_default

  depends_on = [pnap_ssh_key.ssh-key,]
}

data "pnap_server" "BMCNodes" {
  hostname                    = var.node_hostname        
}

output "BMCNodes_firstips"{
  value                       = data.pnap_server.BMCNodes.primary_ip_address
}

output "BMCNodes_allips"{
  value                       = pnap_server.BMCNodes.public_ip_addresses
}
